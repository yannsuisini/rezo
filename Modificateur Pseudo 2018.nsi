
Unicode true

!include nsDialogs.nsh
!include LogicLib.nsh
!include "TextFunc.nsh"
!include x64.nsh
!include StrRep.nsh
!include ReplaceInFile.nsh


Name "Modificateur de Pseudo 2018"
OutFile "Modificateur de Pseudo 2018.exe"


; Request application privileges for Windows Vista
RequestExecutionLevel admin
ShowInstDetails show
XPStyle on


Page custom nsDialogsPage
Page instfiles


Var BUTTON
Var EDIT
Var CHECKBOX

Var Dialog
Var Label1
Var Label2
Var Text
Var Pseudo
Var OUTpath

Function nsDialogsPage

	;ReadRegStr $OUTsteampack HKLM SOFTWARE\rezo2018 "steampack"
	;StrCmp $OUTsteampack "" 0 goon
	;MessageBox MB_OK|MB_ICONINFORMATION "KillingFloor n'est pas install? !"
	;Quit

	goon:

	GetDlgItem $0 $HWNDPARENT 1
	EnableWindow $0 0

	nsDialogs::Create 1018
	Pop $Dialog

	${NSD_CreateLabel} 0 0 100% 12u "Pseudo :"
	Pop $Label1

	${NSD_CreateText} 0 13u 100% 10% ""
	Pop $EDIT
	GetFunctionAddress $0 OnChange
	nsDialogs::OnChange $EDIT $0

	${NSD_CreateLabel} 0 40u 75% 40u "* Rentrez le Pseudo ci dessus.$\n* Minimum 3 caractères.$\n* Cliquez ensuite sur Install."
	Pop $Label2


	GetFunctionAddress $0 OnBack
	nsDialogs::OnBack $0

	nsDialogs::Show

FunctionEnd

Function OnClick

	Pop $0 # HWND

FunctionEnd

Function OnChange

	Pop $0 # HWND


	System::Call user32::GetWindowText(i$EDIT,t.r0,i${NSIS_MAX_STRLEN})

	Strlen $1 $0
	StrCpy $Pseudo $0

	GetDlgItem $0 $HWNDPARENT 1
	${If} $1 > 2
		EnableWindow $0 1
	${Else}
		EnableWindow $0 0
	${EndIf}

FunctionEnd

Function OnBack

	MessageBox MB_YESNO "Etes vous sur ?" IDYES +2
	Abort

FunctionEnd


Section
	${If} ${RunningX64}
	   DetailPrint "Version Windows 64-bit"
	${EndIf}

	DetailPrint "Modification du pseudo ($Pseudo)"

	dayofinfamy:
	ReadRegStr $OUTpath HKLM SOFTWARE\rezo2018 "dayofinfamy"
	StrCmp $OUTpath "" golfwithyourfriends
	DetailPrint "Day of Infamy"
	DetailPrint "$OUTpath\Day of Infamy\SmartSteamEmu\smartsteamemu.ini"
	${ConfigWrite} "$OUTpath\Day of Infamy\SmartSteamEmu\smartsteamemu.ini" "PersonaName =" " $Pseudo" $R0
	;${ConfigWrite} "$OUTpath\Day of Infamy\SmartSteamEmu\smartsteamemu.ini" "SteamIdGeneration =" " ip" $R0
	${ConfigWrite} "$OUTpath\Day of Infamy\SmartSteamEmu\smartsteamemu.ini" "Language =" " french" $R0

	golfwithyourfriends:
	ReadRegStr $OUTpath HKLM SOFTWARE\rezo2018 "golfwithyourfriends"
	StrCmp $OUTpath "" humanfallflat
	DetailPrint "Golf With Your Friends"
	DetailPrint "$OUTpath\Golf With Your Friends\SmartSteamEmu\smartsmartsteamemu.ini"
	${ConfigWrite} "$OUTpath\Golf With Your Friends\SmartSteamEmu\smartsmartsteamemu.ini" "PersonaName =" " $Pseudo" $R0
	;${ConfigWrite} "$OUTpath\Golf With Your Friends\SmartSteamEmu\smartsmartsteamemu.ini" "SteamIdGeneration =" " ip" $R0
	${ConfigWrite} "$OUTpath\\Golf With Your Friends\SmartSteamEmu\smartsmartsteamemu.ini" "Language =" " french" $R0

	humanfallflat:
	ReadRegStr $OUTpath HKLM SOFTWARE\rezo2018 "humanfallflat"
	StrCmp $OUTpath "" rainbowsixsiege
	DetailPrint "Human Fall Flat"
	DetailPrint "$OUTpath\Human Fall Flat\SmartSteamEmu\SmartSteamEmu.ini"
	${ConfigWrite} "$OUTpath\Human Fall Flat\SmartSteamEmu\SmartSteamEmu.ini" "PersonaName =" " $Pseudo" $R0
	;${ConfigWrite} "$OUTpath\Human Fall Flat\SmartSteamEmu\SmartSteamEmu.ini" "SteamIdGeneration =" " ip" $R0
	${ConfigWrite} "$OUTpath\Human Fall Flat\SmartSteamEmu\SmartSteamEmu.ini" "Language =" " french" $R0
	;!insertmacro _ReplaceInFile "$OUTpath\payday 2\Profile.ini" "SadE" "$Pseudo"

	rainbowsixsiege:
	ReadRegStr $OUTpath HKLM SOFTWARE\rezo2018 "rainbowsixsiege"
	StrCmp $OUTpath "" end
	DetailPrint "Rainbow Six Siege"
	DetailPrint "$OUTpath\Rainbow Six Siege\Codex.ini"
	${ConfigWrite} "$OUTpath\Rainbow Six Siege\codex.ini" "UserName=" "$Pseudo" $R0

	roadredemption:
	ReadRegStr $OUTpath HKLM SOFTWARE\rezo2018 "roadredemption"
	StrCmp $OUTpath "" wreckfest
	DetailPrint "Road Redemption"
		
	wreckfest:
	ReadRegStr $OUTpath HKLM SOFTWARE\rezo2018 "wreckfest"
	StrCmp $OUTpath "" end
	DetailPrint "WreckFest"
	end:

SectionEnd
