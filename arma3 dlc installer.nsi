; example1.nsi
;
; This script is perhaps one of the simplest NSIs you can make. All of the
; optional settings are left to their default settings. The installer simply
; prompts the user asking them where to install, and drops a copy of example1.nsi
; there.

;--------------------------------

!include "StrFunc.nsh"
${StrRep}

; The name of the installer
Name "Arma3 DLC Installer"

; The file to write
OutFile "Arma3 DLC Installer.exe"

; Request application privileges for Windows Vista
RequestExecutionLevel admin
ShowInstDetails show
AllowRootDirInstall true
; The default installation directory
;InstallDir "C:\"

; The text to prompt the user to enter a directory
;DirText "Choisir la racine du disque de destination "


;--------------------------------

Var OUTutorrent
Var OUTSteam

; The stuff to install
Section "" ;No components page, name is not important
	DetailPrint "Check pour v�rifier que utorrent est install�"
	ReadRegStr $OUTutorrent HKLM "SOFTWARE\Rezo2018" "utorrent"
	StrCmp $OUTutorrent "" goon goon
	MessageBox MB_OK|MB_ICONINFORMATION "uTorrent n'est pas install� !"
	Quit
	
	goon:
	DetailPrint "OK."
	DetailPrint "Check pour v�rifier que Steam est install�"
	ReadRegStr $OUTsteam HKCU "SOFTWARE\Valve\Steam" "SteamPath"
	StrCmp $OUTsteam "" 0 goon2
	MessageBox MB_OK|MB_ICONINFORMATION "Steam n'est pas install� !"
	Quit
	
	goon2:
	${StrRep} $OUTsteam $OUTsteam  "/" "\"
	DetailPrint "OK."
	DetailPrint "Check pour v�rifier que Arma 3 est install�"
	IfFileExists "$OUTsteam\steamapps\common\arma 3\arma3.exe" goon3 0
	MessageBox MB_OK "Arma 3 n'est pas install� !"
	Quit
	
	goon3:
	DetailPrint "Chemin Arma 3:"
	DetailPrint "$OUTsteam\steamapps\common\arma 3"
	DetailPrint "On lance le torrent d'Arma3 afin d'installer les DLC."
	DetailPrint "Apr�s la v�rification de utorrent"
	DetailPrint "Faire �ventuellement un 'force download' sur le torrent d'Arma3"

	Setoutpath "$OUTutorrent\uTorrent"
	File /a  "additional_torrents\Ground Branch.torrent"
	Exec '$OUTutorrent\uTorrent\utorrent.exe /noinstall /directory "$OUTsteam\steamapps\common" "$OUTutorrent\uTorrent\arma 3.torrent"'
	
	
SectionEnd ; end the section
