; example1.nsi
;
; This script is perhaps one of the simplest NSIs you can make. All of the
; optional settings are left to their default settings. The installer simply
; prompts the user asking them where to install, and drops a copy of example1.nsi
; there.

;--------------------------------

!include "StrFunc.nsh"
${StrRep}

; The name of the installer
Name "Arma3 Unlocker"

; The file to write
OutFile "Arma3 Unlocker.exe"

; Request application privileges for Windows Vista
RequestExecutionLevel admin
ShowInstDetails show
AllowRootDirInstall true
; The default installation directory
;InstallDir "C:\"

; The text to prompt the user to enter a directory
;DirText "Choisir la racine du disque de destination "


;--------------------------------

Var OUTutorrent
Var OUTSteam

; The stuff to install
Section "" ;No components page, name is not important
	DetailPrint "Check pour v�rifier que Steam est install�"
	ReadRegStr $OUTsteam HKCU "SOFTWARE\Valve\Steam" "SteamPath"
	StrCmp $OUTsteam "" 0 goon2
	MessageBox MB_OK|MB_ICONINFORMATION "Steam n'est pas install� !"
	Quit
	
	goon2:
	${StrRep} $OUTsteam $OUTsteam  "/" "\"
	DetailPrint "OK."
	DetailPrint "Check pour v�rifier que Arma 3 est install�"
	IfFileExists "$OUTsteam\steamapps\common\Arma 3\arma3.exe" goon3 0
	MessageBox MB_OK "Arma 3 n'est pas install� !"
	Quit
	
	goon3:
	DetailPrint "Chemin Arma 3:"
	DetailPrint "$OUTsteam\steamapps\common\Arma 3"
	DetailPrint "Copie des fichiers..."
	Setoutpath "$OUTsteam\steamapps\common\Arma 3"
	File /a  creamapi\*.*
	DetailPrint "OK..."
		
	
SectionEnd ; end the section
