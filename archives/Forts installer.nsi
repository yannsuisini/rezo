; example1.nsi
;
; This script is perhaps one of the simplest NSIs you can make. All of the
; optional settings are left to their default settings. The installer simply
; prompts the user asking them where to install, and drops a copy of example1.nsi
; there.

;--------------------------------

; The name of the installer
Name "Forts Installer"

; The file to write
OutFile "Forts Installer.exe"

; Request application privileges for Windows Vista
RequestExecutionLevel user
ShowInstDetails show
AllowRootDirInstall true
; The default installation directory
InstallDir "C:\"

; The text to prompt the user to enter a directory
DirText "Choisir la racine du disque de destination "


;--------------------------------

Var OUTutorrent

; The stuff to install
Section "" ;No components page, name is not important
	AddSize 500000
	ReadRegStr $OUTutorrent HKLM "SOFTWARE\Rezo2017" "utorrent"
	StrCmp $OUTutorrent "" 0 goon
	MessageBox MB_OK|MB_ICONINFORMATION "uTorrent n'est pas installé !"
	Quit

	goon:
	Setoutpath "$OUTutorrent\uTorrent\icones"
	File /a  additional_icons\forts.ico
	Setoutpath "$OUTutorrent\uTorrent"
	File /a  additional_torrents\forts.torrent

	Exec '$OUTutorrent\uTorrent\utorrent.exe /noinstall /directory "$INSTDIR" "$OUTutorrent\uTorrent\forts.torrent"'
	WriteRegStr HKLM "Software\Rezo2017\" "forts" "$INSTDIR"

	Setoutpath "$INSTDIR\Forts\"
	CreateShortCut "$DESKTOP\Rezo2017\Forts.lnk" "$INSTDIR\Forts\SmartSteamLoader.exe" "" "$OUTutorrent\utorrent\icones\forts.ico" 0


SectionEnd ; end the section
