; example1.nsi
;
; This script is perhaps one of the simplest NSIs you can make. All of the
; optional settings are left to their default settings. The installer simply 
; prompts the user asking them where to install, and drops a copy of example1.nsi
; there. 

;--------------------------------

; The name of the installer
Name "UT3 - Map Install"

; The file to write
OutFile "install ut3 maps.exe"

; Request application privileges for Windows Vista
RequestExecutionLevel user
ShowInstDetails show

;--------------------------------

; Pages

Page instfiles

;--------------------------------

Var OUTut3

; The stuff to install
Section "" ;No components page, name is not important

  	ReadRegStr $OUTut3 HKLM "SOFTWARE\Rezo2014" "ut3"
	StrCmp $OUTut3 "" 0 goon
	MessageBox MB_OK|MB_ICONINFORMATION "UT3 n'est pas install� !"
	Quit
	goon:
	Createdirectory "$DOCUMENTS\My Games\Unreal Tournament 3\UTGame\Published\CookedPC\CustomMaps"
	SetOutPath "$DOCUMENTS\My Games\Unreal Tournament 3\UTGame\Published\CookedPC\CustomMaps"
	File /r ut3\*.*
	
	
SectionEnd ; end the section
