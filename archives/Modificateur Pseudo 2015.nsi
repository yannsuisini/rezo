!include nsDialogs.nsh
!include LogicLib.nsh
!include "TextFunc.nsh"
!include x64.nsh
!include StrRep.nsh
!include ReplaceInFile.nsh


Name "Modificateur de Pseudo 2015"
OutFile "Modificateur de Pseudo 2015.exe"

; Request application privileges for Windows Vista
RequestExecutionLevel admin
ShowInstDetails show
XPStyle on

Page custom nsDialogsPage
Page instfiles


Var BUTTON
Var EDIT
Var CHECKBOX

Var Dialog
Var Label1
Var Label2
Var Text
Var Pseudo
Var OUTpath

Function nsDialogsPage

	;ReadRegStr $OUTsteampack HKLM SOFTWARE\rezo2015 "steampack"
	;StrCmp $OUTsteampack "" 0 goon
	;MessageBox MB_OK|MB_ICONINFORMATION "KillingFloor n'est pas install� !"
	;Quit
	
	goon:

	GetDlgItem $0 $HWNDPARENT 1
	EnableWindow $0 0

	nsDialogs::Create 1018
	Pop $Dialog
	
	${NSD_CreateLabel} 0 0 100% 12u "Pseudo :"
	Pop $Label1

	${NSD_CreateText} 0 13u 100% 10% ""
	Pop $EDIT
	GetFunctionAddress $0 OnChange
	nsDialogs::OnChange $EDIT $0
	
	${NSD_CreateLabel} 0 40u 75% 40u "* Rentrez le Pseudo ci dessus.$\n* Minimum 3 caract�res.$\n* Cliquez ensuite sur Install."
	Pop $Label2


	GetFunctionAddress $0 OnBack
	nsDialogs::OnBack $0

	nsDialogs::Show

FunctionEnd

Function OnClick

	Pop $0 # HWND

FunctionEnd

Function OnChange

	Pop $0 # HWND
	

	System::Call user32::GetWindowText(i$EDIT,t.r0,i${NSIS_MAX_STRLEN})

	Strlen $1 $0
	StrCpy $Pseudo $0

	GetDlgItem $0 $HWNDPARENT 1	
	${If} $1 > 2
		EnableWindow $0 1	
	${Else}
		EnableWindow $0 0
	${EndIf}

FunctionEnd

Function OnBack

	MessageBox MB_YESNO "Etes vous sur ?" IDYES +2
	Abort

FunctionEnd


Section
	${If} ${RunningX64}
	   DetailPrint "Version Windows 64-bit"
	${EndIf}

	DetailPrint "Modification du pseudo ($Pseudo)" 
	
	gasguzzlers:
;	ReadRegStr $OUTpath HKLM SOFTWARE\rezo2015 "gasguzzlers"
;	StrCmp $OUTpath "" sniperelite3
;	DetailPrint "Gas Guzzlers Extreme"
;	DetailPrint "$OUTpath\GasGuzzlersExtreme\bin32\smartsteamemu.ini" 
;	${ConfigWrite} "$OUTpath\GasGuzzlersExtreme\bin32\smartsteamemu.ini" "PersonaName =" " $Pseudo" $R0
;	${ConfigWrite} "$OUTpath\GasGuzzlersExtreme\bin32\smartsteamemu.ini" "SteamIdGeneration =" " ip" $R0
;	${ConfigWrite} "$OUTpath\GasGuzzlersExtreme\bin32\smartsteamemu.ini" "Language =" " french" $R0
;	
;	sniperelite3:
;	ReadRegStr $OUTpath HKLM SOFTWARE\rezo2015 "sniperelite3"
;	StrCmp $OUTpath "" skydrift
;	DetailPrint "Sniper Elite 3"
;	DetailPrint "$OUTpath\Sniper Elite 3\smartsteamemu.ini" 
;	${ConfigWrite} "$OUTpath\Sniper Elite 3\smartsteamemu.ini" "PersonaName =" " $Pseudo" $R0
;	${ConfigWrite} "$OUTpath\Sniper Elite 3\smartsteamemu.ini" "SteamIdGeneration =" " ip" $R0
;	${ConfigWrite} "$OUTpath\Sniper Elite 3\smartsteamemu.ini" "Language =" " french" $R0
;	
;	skydrift:
;	ReadRegStr $OUTpath HKLM SOFTWARE\rezo2015 "skydrift"
;	StrCmp $OUTpath "" arma3
;	DetailPrint "Skydrift"
;	DetailPrint "$OUTpath\Skydrift\smartsteamemu.ini" 
;	${ConfigWrite} "$OUTpath\Skydrift\smartsteamemu.ini" "PersonaName =" " $Pseudo" $R0
;	${ConfigWrite} "$OUTpath\Skydrift\smartsteamemu.ini" "SteamIdGeneration =" " ip" $R0
;	${ConfigWrite} "$OUTpath\Skydrift\smartsteamemu.ini" "Language =" " french" $R0
;	;!insertmacro _ReplaceInFile "$OUTpath\payday 2\Profile.ini" "SadE" "$Pseudo"
;
;	arma3:
;	ReadRegStr $OUTpath HKLM SOFTWARE\rezo2015 "arma3"
;	StrCmp $OUTpath "" ut3
;	DetailPrint "Arma 3"
;	DetailPrint "$OUTpath\Arma 3\LumaEmu.ini" 
;
;	${ConfigWrite} "$OUTpath\Arma 3\LumaEmu.ini" "PlayerName =" " $Pseudo" $R0
;	${ConfigWrite} "$OUTpath\Arma 3\LumaEmu.ini" "PlayerNickname =" " $Pseudo" $R0
;	${ConfigWrite} "$OUTpath\Arma 3\LumaEmu.ini" "PlayerName =" " $Pseudo" $R0
;	${ConfigWrite} "$OUTpath\Arma 3\LumaEmu.ini" "ClanName =" " Rezo2015" $R0
;	${ConfigWrite} "$OUTpath\Arma 3\LumaEmu.ini" "ClanTag =" " [RzO15]" $R0
;	
;	ut3:
;	ReadRegStr $OUTpath HKLM SOFTWARE\rezo2015 "ut3"
;	StrCmp $OUTpath "" ss3
	DetailPrint "UT3"
	Createdirectory "$DOCUMENTS\My Games\Unreal Tournament 3\UTGame\SaveData"
	SetOutPath "$DOCUMENTS\My Games\Unreal Tournament 3\UTGame\SaveData"
	File  pseudo\SadE.ue3profile
	Rename "$DOCUMENTS\My Games\Unreal Tournament 3\UTGame\SaveData\SadE.ue3profile" "$DOCUMENTS\My Games\Unreal Tournament 3\UTGame\SaveData\$Pseudo.ue3profile"
;	
;	ss3:
;	ReadRegStr $OUTpath HKLM SOFTWARE\rezo2015 "ss3"
;	StrCmp $OUTpath "" end
;	DetailPrint "Serious Sam 3"
;	DetailPrint "$OUTpath\Serious Sam 3\bin\smartsteamemu.ini" 
;	${ConfigWrite} "$OUTpath\Serious Sam 3\bin\smartsteamemu.ini" "PersonaName =" " $Pseudo" $R0
;	${ConfigWrite} "$OUTpath\Serious Sam 3\bin\smartsteamemu.ini" "SteamIdGeneration =" " ip" $R0
;	${ConfigWrite} "$OUTpath\Serious Sam 3\bin\smartsteamemu.ini" "Language =" " french" $R0
	
	end:

SectionEnd
