; example1.nsi
;
; This script is perhaps one of the simplest NSIs you can make. All of the
; optional settings are left to their default settings. The installer simply 
; prompts the user asking them where to install, and drops a copy of example1.nsi
; there. 

;--------------------------------

; The name of the installer
Name "Toxikk Installer"

; The file to write
OutFile "Toxikk Installer.exe"

; Request application privileges for Windows Vista
RequestExecutionLevel user
ShowInstDetails show
AllowRootDirInstall true
; The default installation directory
InstallDir "C:\"

; The text to prompt the user to enter a directory
DirText "Choisir la racine du disque de destination "


;--------------------------------

Var OUTutorrent

; The stuff to install
Section "" ;No components page, name is not important
	AddSize 1500233
	
	ReadRegStr $OUTutorrent HKLM "SOFTWARE\Rezo2015" "utorrent"
	StrCmp $OUTutorrent "" 0 goon
	MessageBox MB_OK|MB_ICONINFORMATION "uTorrent n'est pas install� !"
	Quit
	
	goon:
	Exec '$OUTutorrent\uTorrent\utorrent.exe /noinstall /directory "$INSTDIR" "$OUTutorrent\uTorrent\Toxikk.torrent"'
	WriteRegStr HKLM "Software\Rezo2015\" "toxikk" "$INSTDIR"
	
	Setoutpath "$INSTDIR\Toxikk\"
	CreateShortCut "$DESKTOP\Rezo2015\Toxikk.lnk" "$INSTDIR\TOXIKK\Binaries\Win32\TOXIKK.exe" "?FullscreenLaunch -NOSTEAM" "$OUTutorrent\utorrent\icones\toxikk.ico" 0
	
	
SectionEnd ; end the section

