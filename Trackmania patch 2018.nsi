; example1.nsi
;
; This script is perhaps one of the simplest NSIs you can make. All of the
; optional settings are left to their default settings. The installer simply
; prompts the user asking them where to install, and drops a copy of example1.nsi
; there.


;--------------------------------

!include nsDialogs.nsh
!include LogicLib.nsh
!include "TextFunc.nsh"
!include x64.nsh
!include StrRep.nsh
!include ReplaceInFile.nsh

; The name of the installer
Name "Trackmania - patch"

; The file to write
OutFile "Trackmania patch.exe"

; Request application privileges for Windows Vista
RequestExecutionLevel admin
ShowInstDetails show

;--------------------------------

; Pages

Page instfiles

;--------------------------------

Var OUTtrackmania

; The stuff to install
Section "" ;No components page, name is not important

  	ReadRegStr $OUTtrackmania HKLM "SOFTWARE\Rezo2018" "trackmania"
	StrCmp $OUTtrackmania "" 0 goon
	MessageBox MB_OK|MB_ICONINFORMATION "Trackmania n'est pas installé !"
	Quit

	goon:
	DetailPrint "Modification de $OUTtrackmania\ManiaPlanet\Nadeo.ini"
	${ConfigWrite} "$OUTtrackmania\ManiaPlanet\Nadeo.ini" "Userdir=" "$OUTtrackmania\ManiaPlanet\donnees_perso" $R0
	${ConfigWrite} "$OUTtrackmania\ManiaPlanet\Nadeo.ini" "Commondir=" "$OUTtrackmania\ManiaPlanet\donnees_communes" $R0
	DetailPrint "Chemins modifiés"


SectionEnd ; end the section
