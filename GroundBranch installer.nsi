; example1.nsi
;
; This script is perhaps one of the simplest NSIs you can make. All of the
; optional settings are left to their default settings. The installer simply
; prompts the user asking them where to install, and drops a copy of example1.nsi
; there.

;--------------------------------

; The name of the installer
Name "Ground Branch Installer"

; The file to write
OutFile "Ground Branch Installer.exe"

; Request application privileges for Windows Vista
RequestExecutionLevel user
ShowInstDetails show
AllowRootDirInstall true
; The default installation directory
InstallDir "C:\"

; The text to prompt the user to enter a directory
DirText "Choisir la racine du disque de destination "


;--------------------------------

Var OUTutorrent

; The stuff to install
Section "" ;No components page, name is not important
	AddSize 18494910
	ReadRegStr $OUTutorrent HKLM "SOFTWARE\Rezo2018" "utorrent"
	StrCmp $OUTutorrent "" 0 goon
	MessageBox MB_OK|MB_ICONINFORMATION "uTorrent n'est pas install� !"
	Quit

	goon:
	Setoutpath "$OUTutorrent\uTorrent\icones"
	File /a  "additional_icons\groundbranch.ico"
	Setoutpath "$OUTutorrent\uTorrent"
	File /a  "additional_torrents\Ground Branch.torrent"

	Exec '$OUTutorrent\uTorrent\utorrent.exe /noinstall /directory "$INSTDIR" "$OUTutorrent\uTorrent\Ground Branch.torrent"'
	WriteRegStr HKLM "Software\Rezo2018\" "groundbranch" "$INSTDIR"

	Setoutpath "$INSTDIR\Ground Branch\"
	CreateShortCut "$DESKTOP\Rezo2018\Ground Branch.lnk" "$INSTDIR\Ground Branch\Groundbranch.exe" "" "$OUTutorrent\utorrent\icones\groundbranch.ico" 0


SectionEnd ; end the section
